package com.zoro.annuaire.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author emmanueltombo
 *
 * @param <T>
 */
public class HttpResponse {

	public String status;
	public String message="";
	public Object data;

	private HttpResponse() {
	}

	public static HttpResponse builder() {
		return new HttpResponse();
	}

	public HttpResponse status(String status) {
		this.status = status;
		return this;
	}

	public HttpResponse message(String message) {
		this.message = message;
		return this;
	}


	public HttpResponse data(Object data) {
		this.data = data;
		return this;
	}

	public String build() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "{}";
	}

}
