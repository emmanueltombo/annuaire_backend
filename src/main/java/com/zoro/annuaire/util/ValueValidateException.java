package com.zoro.annuaire.util;

public class ValueValidateException extends Exception {
	public ValueValidateException(String message) {
		super(message);
	}
}
