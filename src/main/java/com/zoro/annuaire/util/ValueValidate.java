package com.zoro.annuaire.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValueValidate {
	public static void checkEmail(String email, String message) throws ValueValidateException {
		Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = emailPattern.matcher(email);
		if (!matcher.find()) {
			throw new ValueValidateException(message);
		}
	}

	public static void checkPhoneNumber(String phone, String message) throws ValueValidateException {
		Pattern phonePattern = Pattern.compile("^\\+(33|243)[0-9]{9}$");
		Matcher matcher = phonePattern.matcher(phone);
		if (!matcher.find()) {
			throw new ValueValidateException(message);
		}
	}

	public static void checkStringValue(String data, String message) throws ValueValidateException {
		if (data==null || data.isEmpty())
			throw new ValueValidateException(message);
	}

	public static void checkPasswordLength(String password, int length, String message) throws ValueValidateException {
		if (password==null || password.length() < length) {
			throw new ValueValidateException(message);
		}
	}
}
