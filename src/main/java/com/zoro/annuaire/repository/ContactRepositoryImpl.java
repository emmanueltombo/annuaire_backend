package com.zoro.annuaire.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zoro.annuaire.model.Contact;
import com.zoro.annuaire.model.User;

@Repository
@Transactional
public class ContactRepositoryImpl implements ContactRepository {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Contact> findAllByUser(User user) {
		try {
			Query query = em.createQuery("SELECT c FROM Contact c join c.user u ON u.id=:user_id", Contact.class);
			query.setParameter("user_id", user.getId());
			List<Contact> contacts = query.getResultList();
			return contacts;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Contact> findContacts(long user_id, String contact_desc) {
		try {
			Query query = em.createQuery(
					"SELECT c FROM Contact c join c.user u WHERE u.id=:user_id and (c.nom like (:contact_desc) or c.prenom like (:contact_desc) or c.telephone like (:contact_desc))",
					Contact.class);
			query.setParameter("user_id", user_id);
			query.setParameter("contact_desc", "%"+contact_desc + "%");
			List<Contact> contacts = query.getResultList();
			return contacts;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Contact save(Contact contact) {
		try {
			em.persist(contact);
			return contact;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Contact update(Contact contact) {
		try {
			User user=em.find(User.class, contact.getUser().getId());
			contact.setUser(user);
			em.merge(contact);
			return contact;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	@Override
	public long delete(Contact contact) {
		try {
			Contact c = em.find(Contact.class, contact.getId());
			em.remove(c);
			return 1;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return 0;
	}

}
