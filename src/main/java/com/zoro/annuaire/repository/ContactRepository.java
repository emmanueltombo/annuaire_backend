package com.zoro.annuaire.repository;

import java.util.List;

import com.zoro.annuaire.model.Contact;
import com.zoro.annuaire.model.User;

/**
 * 
 * @author emmanueltombo
 *
 */

public interface ContactRepository {

	public List<Contact> findAllByUser(User user);
	
	public List<Contact> findContacts(long user_id,String contact_desc);

	public Contact save(Contact contact);

	public Contact update(Contact contact);

	public long delete(Contact contact);
}
