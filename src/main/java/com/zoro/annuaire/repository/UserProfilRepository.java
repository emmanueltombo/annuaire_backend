package com.zoro.annuaire.repository;

import org.springframework.stereotype.Repository;

import com.zoro.annuaire.model.User;

@Repository
public interface UserProfilRepository {
	public User create(User user);
	public User get(User user);
	public User update(User user);
	public User check(User user);

}
