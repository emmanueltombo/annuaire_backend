package com.zoro.annuaire.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zoro.annuaire.model.User;

@Repository
@Transactional
public class UserProfilRepositoryImpl implements UserProfilRepository {

	@PersistenceContext
	EntityManager em;
	
	public UserProfilRepositoryImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public User create(User user) {
		try {
			em.persist(user);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User get(User user) {
		try {
			Query query = em.createQuery("SELECT u FROM User u WHERE u.username=:username and u.password=:password",
					User.class);
			query.setParameter("username", user.getUsername());
			query.setParameter("password", user.getPassword());

			List<User> users = query.getResultList();
			if (!users.isEmpty())
				return users.get(0);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User check(User user) {
		try {
			Query query = em.createQuery("SELECT u FROM User u WHERE u.username=:username",
					User.class);
			query.setParameter("username", user.getUsername());

			List<User> users = query.getResultList();
			if (!users.isEmpty())
				return users.get(0);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User update(User user) {
		try {
			em.unwrap(Session.class).merge(user);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
