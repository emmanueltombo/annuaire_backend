package com.zoro.annuaire.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zoro.annuaire.dto.UserDto;
import com.zoro.annuaire.model.User;
import com.zoro.annuaire.service.UserProfilService;
import com.zoro.annuaire.util.HttpResponse;
import com.zoro.annuaire.util.UserProfilDataValidate;
import com.zoro.annuaire.util.ValueValidate;
import com.zoro.annuaire.util.ValueValidateException;

/**
 * 
 * @author emmanueltombo
 *
 */

@RestController
@RequestMapping(path = "/auth", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserProfilController {
	@Autowired
	UserProfilService profilService;

	@PostMapping(value = "/signup")
	public String signup(@RequestBody User user) {
		if (user != null) {
			try {
				UserProfilDataValidate.dataValidate(new ModelMapper().map(user, UserDto.class));
				if (this.profilService.checkUserProfil(user))
					throw new ValueValidateException("Le nom d'utilisateur est déjà utilisé par un autre compte");

				UserDto userSignup = this.profilService.signup(user);
				if (userSignup != null) {
					return HttpResponse.builder().status("200").data(userSignup).build();
				} else {
					return HttpResponse.builder().status("400").message("Echec d'enregistrement").build();
				}

			} catch (Exception e) {
				return HttpResponse.builder().status("400").message(e.getMessage()).build();
			}

		}
		return HttpResponse.builder().status("400").message("Impossible de créer un compte").build();
	}

	@PostMapping(value = "/login")
	public String login(@RequestBody UserDto user) {
		if (user != null) {
			try {
				UserProfilDataValidate.loginDataValidate(user);
				UserDto userLogin = this.profilService.login(user);
				if (userLogin != null) {
					return HttpResponse.builder().status("200").data(userLogin).build();
				} else {
					return HttpResponse.builder().status("400").message("Aucun profil trouvé").build();
				}

			} catch (Exception e) {
				return HttpResponse.builder().status("400").message(e.getMessage()).build();
			}
		}
		return HttpResponse.builder().status("400").message("Aucun profil trouvé").build();
	}
	
	@PutMapping
	public String updateUser(@RequestBody UserDto user) {
		if (user != null) {
			try {
				UserProfilDataValidate.dataValidate(user);
				
				UserDto userLogin = this.profilService.updateUserProfil(user);
				if (userLogin != null) {
					return HttpResponse.builder().status("200").data(userLogin).build();
				} else {
					return HttpResponse.builder().status("400").message("Echec de modification").build();
				}

			} catch (Exception e) {
				return HttpResponse.builder().status("400").message(e.getMessage()).build();
			}
		}
		return HttpResponse.builder().status("400").message("Aucune données à modifier").build();
	}

}
