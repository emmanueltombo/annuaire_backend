package com.zoro.annuaire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoro.annuaire.dto.ContactDto;
import com.zoro.annuaire.model.Contact;
import com.zoro.annuaire.service.ContactService;
import com.zoro.annuaire.util.ContactDataValidate;
import com.zoro.annuaire.util.HttpResponse;
import com.zoro.annuaire.util.ValueValidate;

@RestController
@RequestMapping(path = "/contact", produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactController {

	@Autowired
	private ContactService contactService;

	@GetMapping(value = "/{user_id}")
	public @ResponseBody String getContacts(@PathVariable("user_id") Long user_id) {

		Iterable<Contact> contacts = this.contactService.findAll(user_id);
		return HttpResponse.builder().status("200").data(contacts).build();

	}

	@GetMapping(value = "/{user_id}/{contact_desc}")
	public @ResponseBody String getContact(@PathVariable("user_id") Long user_id,
			@PathVariable("contact_desc") String contact_desc) {
		
		System.out.println(contact_desc);

		Iterable<Contact> contacts = this.contactService.findContact(user_id, contact_desc);
		return HttpResponse.builder().status("200").data(contacts).build();

	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public String newContact(@RequestBody ContactDto contactDto) {
System.out.println(contactDto);
		if (contactDto != null) {

			try {
				ContactDataValidate.dataValue(contactDto);

				Contact contactSaved = this.contactService.save(contactDto);
				if (contactSaved != null) {
					return HttpResponse.builder().status("200").message("").data(contactDto).build();
				} else {
					return HttpResponse.builder().status("400").message("Echec d'enregistremeent").build();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return HttpResponse.builder().status("400").message(e.getMessage()).build();
			}

		}
		return HttpResponse.builder().status("400").message("Aucune donnée à enregistrer").build();

	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateContact(@RequestBody ContactDto contactDto) {

		if (contactDto != null) {

			try {
				ContactDataValidate.dataValue(contactDto);
				Contact contactSaved = this.contactService.update(contactDto);
				if (contactSaved != null) {
					return HttpResponse.builder().status("200").message("").data(contactDto).build();
				} else {
					return HttpResponse.builder().status("400").message("Echec de modification").build();
				}
			} catch (Exception e) {
				return HttpResponse.builder().status("400").message(e.getMessage()).build();
			}

		}
		return HttpResponse.builder().status("400").message("Aucune donnée trouvée").build();

	}

	@DeleteMapping(value = "/{contact_id}")
	public String deleteContact(@PathVariable("contact_id") long contact_id) {
		System.out.println("contact_id:" + contact_id);
		try {
			boolean contactSaved = this.contactService.delete(new ContactDto(contact_id));
			if (contactSaved) {
				return HttpResponse.builder().status("200").message("").build();
			} else {
				return HttpResponse.builder().status("400").message("Echec de suppression").build();
			}
		} catch (Exception e) {
			return HttpResponse.builder().status("400").message(e.getMessage()).build();
		}

	}

}
