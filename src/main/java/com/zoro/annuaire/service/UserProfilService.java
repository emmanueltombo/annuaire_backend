package com.zoro.annuaire.service;

import com.zoro.annuaire.dto.UserDto;
import com.zoro.annuaire.model.User;

public interface UserProfilService {

	public UserDto signup(User user);

	public UserDto login(UserDto user);

	public UserDto updateUserProfil(UserDto user);

	public boolean checkUserProfil(User user);
}
