package com.zoro.annuaire.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.zoro.annuaire.dto.ContactDto;
import com.zoro.annuaire.model.Contact;
import com.zoro.annuaire.model.User;
import com.zoro.annuaire.repository.ContactRepository;

@Controller
public class ContactServiceImlp implements ContactService {

	@Autowired
	private ContactRepository contactRepository;

	@Override
	public List<Contact> findAll(long user_id) {
		return this.contactRepository.findAllByUser(new User(user_id));
	}

	@Override
	public Contact save(ContactDto contactDto) {
		ModelMapper modelMapper = new ModelMapper();
		Contact contact = modelMapper.map(contactDto, Contact.class);
		User user = new User(contactDto.getUser_id());
		contact.setUser(user);
		return this.contactRepository.save(contact);
	}

	@Override
	public Contact update(ContactDto contactDto) {
		ModelMapper modelMapper = new ModelMapper();
		Contact contact = modelMapper.map(contactDto, Contact.class);
		contact.setUser(new User(contactDto.getUser_id()));
		return this.contactRepository.update(contact);
	}

	@Override
	public boolean delete(ContactDto contactDto) {
		ModelMapper modelMapper = new ModelMapper();
		Contact contact = modelMapper.map(contactDto, Contact.class);
		long rep = this.contactRepository.delete(contact);
		if (rep == 1)
			return true;

		return false;
	}

	@Override
	public List<Contact> findContact(long user_id,String contact_desc) {
		return this.contactRepository.findContacts(user_id,contact_desc);
	}

}
