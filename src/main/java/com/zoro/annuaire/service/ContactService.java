package com.zoro.annuaire.service;

import java.util.List;

import com.zoro.annuaire.dto.ContactDto;
import com.zoro.annuaire.model.Contact;

public interface ContactService {
	public List<Contact> findAll(long user_id);
	
	public List<Contact> findContact(long user_id,String contact_desc);

	public Contact save(ContactDto contactDto);

	public Contact update(ContactDto contactDto);

	public boolean delete(ContactDto contactDto);
}
