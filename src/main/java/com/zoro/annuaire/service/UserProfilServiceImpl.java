package com.zoro.annuaire.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoro.annuaire.dto.UserDto;
import com.zoro.annuaire.model.User;
import com.zoro.annuaire.repository.UserProfilRepository;

@Service
public class UserProfilServiceImpl implements UserProfilService {

	@Autowired
	UserProfilRepository profilRepository;
	private ModelMapper mapper = new ModelMapper();

	@Override
	public UserDto signup(User user) {
		User usr = this.profilRepository.create(user);
		if (usr != null)
			return mapper.map(usr, UserDto.class);
		return null;
	}

	@Override
	public UserDto login(UserDto userDto) {
		User user = mapper.map(userDto, User.class);
		User usr = this.profilRepository.get(user);
		if (usr != null) {
			usr.setPassword("");
			return mapper.map(usr, UserDto.class);
		}
		return null;
	}

	@Override
	public UserDto updateUserProfil(UserDto userDto) {
		User user = mapper.map(userDto, User.class);
		if(userDto.getPassword()==null || userDto.getPassword().isEmpty()) {
			User u = this.profilRepository.check(user);
			user.setPassword(u.getPassword());
		}
		User usr = this.profilRepository.update(user);
		if (usr != null) {
			UserDto us=mapper.map(usr, UserDto.class);
			us.setPassword("");
			return us;
		}
		return null;
	}

	@Override
	public boolean checkUserProfil(User user) {
		User usr = this.profilRepository.check(user);
		if (usr != null)
			return true;
		return false;
	}

}
