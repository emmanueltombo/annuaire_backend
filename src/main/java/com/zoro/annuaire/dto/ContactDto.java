package com.zoro.annuaire.dto;

public class ContactDto {
	public long id;

	public String nom;

	public String prenom;

	public String telephone;

	public String email;

	public String poste;

	public String entreprise;

	public String adresse_entreprise;

	public long user_id;
	
	public ContactDto() {
		// TODO Auto-generated constructor stub
	}

	public ContactDto(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}

	public String getAdresse_entreprise() {
		return adresse_entreprise;
	}

	public void setAdresse_entreprise(String adresse_entreprise) {
		this.adresse_entreprise = adresse_entreprise;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
}
