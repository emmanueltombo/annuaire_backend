package com.zoro.annuaire;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

@SpringBootTest
@ComponentScan({"com.zoro.annuaire.*"})
class AnnuaireApplicationTests {

	@Test
	void contextLoads() {
	}

}
