# Annuaire

## Endpoint server
- Base url: http://185.148.129.99:8099/annuaire

Ci-dessous sont les urls pour la gestion de contacts et d'utilisateurs
### 1) Création du profil utilisateur
Url: http://185.148.129.99:8099/annuaire/user/signup, via POST
#### Format de données à envoyer:
{
    "firstname": "TOMBO",
    "lastname": "Emmanuel",
    "username": "emma",
    "password": "1234"
}
#### Réponse
- {
    "status": "200",
    "message": "",
    "data": {
        "id": 12,
        "firstname": "TOMBO",
        "lastname": "Emmanuel",
        "username": "emma13",
        "password": "1234"
    }
}
 - {
    "status": "400",
    "message": "message",
    "data": null
}

### 2) Login
Url: http://185.148.129.99:8099/annuaire/user/login, via POST
#### Format de données à envoyer
- {
    "username": "emma",
    "password": "1234"
}
#### Réponse
- {
    "status": "200",
    "message": "message",
    "data": {
        "id": 12,
        "firstname": "TOMBO",
        "lastname": "Emmanuel",
        "username": "emma13",
        "password": ""
    }
}
 - {
    "status": "400",
    "message": "Erreur",
    "data": null
}

### 2) Modification de compte utilisateur
Url: http://185.148.129.99:8099/annuaire/user/, via PUT
#### Format de données à envoyer
- {
    "id":2,
    "firstname": "TOMBO",
    "lastname": "Emmanuel",
    "username": "emma",
    "password": "1234"
}
#### Réponse
- {
    "status": "200",
    "message": "message",
    "data": {
        "id": 2,
        "firstname": "TOMBO",
        "lastname": "Emmanuel",
        "username": "emma13",
        "password": ""
    }
}
 - {
    "status": "400",
    "message": "Erreur",
    "data": null
}
### 3) Enregistrer un contact
Url: http://185.148.129.99:8099/annuaire/contact, via POST
#### Format de données à envoyer
- {
    "nom": "TOMBO",
    "prenom": "Emmanuel",
    "telephone": "8765678",
    "email": "e@gmail.com",
    "poste": "SE",
    "entreprise": "CREAS MIT",
    "adresse_entreprise": "Gombe",
    "user_id":1
}
#### Réponse
- {
    "status": "200",
    "message": "",
    "data": {}
}
 - {
    "status": "400",
    "message": "Erreur",
    "data": null
}

### 4) liste de contacts
Url: /user/contact/user_id, via GET

#### Réponse
- {
    "status": "200",
    "message": "",
    "data": []
}

### 4) Rechercher un contact
Url: http://185.148.129.99:8099/annuaire/contact/user_id/contact_desc, via GET

#### Réponse
- {
    "status": "200",
    "message": "",
    "data": []
}

### 4) Modifier un contact
Url: http://185.148.129.99:8099/annuaire/contact, via PUT
#### Format de données à envoyer
- {
- "id":1,
    "nom": "TOMBO",
    "prenom": "Emmanuel",
    "telephone": "8765678",
    "email": "e@gmail.com",
    "poste": "SE",
    "entreprise": "CREAS MIT",
    "adresse_entreprise": "Gombe",
    "user_id":1
}
#### Réponse
- {
    "status": "200",
    "message": "",
    "data": {}
}

### 4) Supprimer un contact
Url: http://185.148.129.99:8099/annuaire/contact/id, via DELETE

#### Réponse
- {
    "status": "200",
    "message": "",
    "data": {}
}



